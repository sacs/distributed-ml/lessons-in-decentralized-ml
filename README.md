# lessons-in-decentralized-ml

Transcript of a talk given internally on my experience building a simulator for Decentralized Machine Learning. 

A video recording of the second half of the talk is available here: https://drive.google.com/file/d/1H9Y8E1qmh1zUfjCOP6jmow6y6ROwuHyP/view?usp=sharing

The slides with notes is available [here](./SaCS-2021-07-19.pdf). Half of the notes were written after the talk so they may not exactly follow the content that was said. The source Keynote file is available [here](./SaCS-2021-07-19.key).
